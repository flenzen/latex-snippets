# My LaTeX packages
This repository contains a few small $\LaTeX$ `.sty` files that I created for my personal needs,
which I want to share because I think they may come in handy also for other people.
These will certainly not be turned into fully-fledged packages with all the nuts and bolts.
Nevertheless, I am happy to answer questions and implement improvements if not too much work.

Concretely, this repository contains:

* _exams/_ is a package collection consisting of
  * _sheet.cls_: my personal style for problem assignments
  * _exam.sty_: package for showing and hiding sample solutions,
    draw (gridded) boxes the participants write their solutions into,
    and for drawing a grading scheme on the title page.
  * _random-identifiers.sty_: individualize exams by reordering subproblems 
    or renaming variables at random.
* _mycv.cls_ used to typeset CVs, including a bibliography
* _mybib.sty_ contains some adjustsments to `biblatex.sty`
* _mymath.sty_ is mostly a long listing of various definitions of symbols and macros that I use all over the place in my writing
* _mybeamer.sty_ contains some helper macros to make my life easier when writing beamer slides
* _mysansmath.sty_ is a small adaption of `sansmath.sty`, also providing a sans bold math version
* _mytheorem-enum.sty_ extends `amsthm.sty` by making working with enumerations in theorems (including references) easier.
* _mytimer.sty_ helps detecting pages with long compile times 

Each of the files is contained in a subfolder of this repository with an own `README.md`.
Please see there for details.

## Usage
Place the files somewhere where latex finds them.
The TEXMFHOME folder is a good place.
If in doubt where that is, ask `kpsewhich --var-value TEXMFHOME`.
You can place the files in a subfolder of that folder.
To make sure that LaTeX finds them, run, e.g., `kpsewhich mycv.cls`.

## Acknowledgements
Please note that many of these files are heavily inspired by solutions from tex.stackexchange.com. 
I cannot reproduce the precise link for each single fragment of code. 
I am sorry for not being able to give appropriate credit everywhere.


