\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mycv}
\newif\ifinlinecontact
\DeclareOption{inlinecontact}{\inlinecontacttrue}
% Pass all options to scrartcl
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions\relax
\LoadClass{scrartcl}

\RequirePackage{csquotes,babel,microtype,graphicx,etoolbox,calc,enumitem,multicol,stackengine}
\RequirePackage[svgnames,hyperref]{xcolor}
\RequirePackage[hidelinks]{hyperref}
\RequirePackage[margin=20mm]{geometry}
\renewcommand{\familydefault}{\sfdefault}

% Add macros address, place, etc. analogously to \author, \title etc.
\newcommand{\@address}{}
\newcommand{\address}[1]{\renewcommand{\@address}{#1}}
\newcommand{\@place}{}
\newcommand{\place}[1]{\renewcommand{\@place}{#1}}
\newcommand{\@photo}{}
\newcommand{\photo}[1]{\renewcommand{\@photo}{#1}}
\newcommand{\@signature}{}
\newcommand{\signature}[1]{\renewcommand{\@signature}{#1}}

% Formatting
% ==========
% Colors use to style the cv
\colorlet{accent}{DarkBlue}
\colorlet{lesser}{gray}
\newcommand{\kw}{\textcolor{accent}}

% Use those colors in styling the document
\setkomafont{descriptionlabel}{\color{accent}}
\setkomafont{disposition}{\normalfont\sffamily\color{accent}}
\setkomafont{section}{\large}
\setkomafont{title}{\Huge}
\setkomafont{subtitle}{\Large}
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\raggedbottom
\raggedcolumns

% Format top level lists. These are mostly description environments with dates in their labels.
% Hence the setting for \descriptionwidthi.
\newcommand{\mydate}[2]{#1\iflanguage{ngerman}{.}{/}#2}
\newlength\descriptionwidthi
\newlength\secindent
\newlength\secrule
\newlength\secruleht
\newlength\secrulesep
\AtBeginDocument{
%	\settowidth\descriptionwidthi{\mydate{12}{2022}}
	\settowidth\descriptionwidthi{\mydate{12}{2022}}
	\settowidth\secrule{\mydate{12}{2022}}
	\setlength\secrulesep{\descriptionsepi}
	\setlength\secruleht{1ex}
}
\newlength\descriptionsepi
\setlength\descriptionsepi{2ex}
\setlist{font=\color{accent}, itemsep=0pt}
\setlist[1]{
	parsep=.75ex,
	align=right,
}
\setlist[2]{before=\small, parsep=0.125pt}
\setlist[description,1]{
	labelwidth=\descriptionwidthi, % Width of a MM.YYYY date
	labelsep=\descriptionsepi,
	leftmargin=!,
}
\setlist[itemize]{labelsep=1ex,labelwidth=\widthof{\labelitemi},leftmargin=!}
% Print date (NN.NNN–NN.NNNN in two lines in the left column)
\NewDocumentCommand{\range}{smm}{%
	\IfBooleanF{#1}{\smash}{\parbox[t]{\descriptionwidthi}{#2\\\llap{–\,}#3}}%
%	\IfBooleanTF{#1}{#2\,–\,#3}{\smash{\parbox[t]{\descriptionwidthi}{#2\\\llap{–\,}#3}}}%
}

%{\smash{\parbox[t]{\descriptionwidthi}{#1\\\llap{–~}#2}}}
\newcommand{\pb}[1]{\smash{\parbox[t]{\descriptionwidthi}{\raggedleft #1}}}
% Style headings by a thick rule in the left column that contains the dates.
\renewcommand\sectionlinesformat[3]{\textcolor{accent}{\hspace{\descriptionwidthi-\secrule}\rule{\secrule}{\secruleht}}\hspace{\secrulesep}#3}
\fboxsep=-\fboxrule

\RedeclareSectionCommand[
afterindent=false,
afterskip=1.00ex plus 0.25ex minus 0.5ex,
beforeskip=1.25ex plus 1.25ex minus 0.5ex
]{section}

\multicolsep=0pt

% Title (name and subtitle on the left, address and photo on the right)
\renewcommand{\titlepagestyle}{empty}
\renewcommand{\maketitle}{%
	\noindent
	\pagestyle{\titlepagestyle}%
	{%
%		\usekomafont{disposition}
		\newsavebox\PersonalInfo%
		\ifinlinecontact
			\savebox\PersonalInfo{%
				\color{lesser}%
				\begin{minipage}[b]{\linewidth-\descriptionsepi-\widthof{\@photo} %-\descriptionwidthi-\secrulesep
				}
					\raggedright
					{\usekomafont{disposition}\usekomafont{title}\@title}\par
					\vspace{1em}%
					\ifx\@subtitle\@empty\else
						{\usekomafont{disposition}\usekomafont{subtitle}\@subtitle}\par
						\vspace{1em}%
					\fi
					\color{lesser}
					\def\\{\discretionary{}{}{\hbox{~\textperiodcentered~}}}
					\@address
				\end{minipage}%
			}
		\else
			\savebox\PersonalInfo{%
				\color{lesser}
				\begin{tabular}[b]{@{}r@{}}%
					\@address%
				\end{tabular}%
			}%
		\fi
%		\hspace{\descriptionwidthi+\secrulesep}%
		\ifinlinecontact
		\else
			\begin{minipage}[b][\ht\PersonalInfo][t]{\widthof{\usekomafont{title}\@title}}
				{\usekomafont{disposition}\usekomafont{title}\@title}\par
				\ifx\@subtitle\@empty
				\else
					\vspace{1em}%
					{\usekomafont{disposition}\usekomafont{subtitle}\@subtitle}\par
				\fi
				\vspace{1em}%
			\end{minipage}%
			\hfill
		\fi
		\usebox{\PersonalInfo}%
		\ifx\@photo\@empty
		\else
			\hspace{\descriptionsepi}%
			\raisebox{-\height+\ht\PersonalInfo}[0pt][0pt]{\@photo}
		\fi
	}%
	\par
	\vspace{1em}%
}

\renewenvironment{abstract}{%
	\begin{addmargin}[0pt]%[\descriptionwidthi+\secrulesep]
		{\descriptionsepi+\widthof{\@photo}}%
		%\hspace{-\descriptionwidthi-\secrulesep}%
		%{\usekomafont{disposition}\usekomafont{section}{\hspace{\descriptionwidthi-\secrule}\rule{\secrule}{\secruleht}\hspace{\secrulesep}}}%
		\ignorespaces
}{%
	\end{addmargin}%
}

% Print signature, place and date
\newcommand\sign{\par\bigskip\@signature\par\medskip\@place, \@date}

% The following is in effect only if biblatex is loaded
\PassOptionsToPackage{giveninits,sorting=ymddnt}{biblatex}
\AfterPackage{biblatex}{
	\DefineBibliographyStrings{german}{%
		phdthesis = {Dissertation}
	}
	\DeclareSourcemap{
		\maps{
			\map{
				\step[fieldset=location, null]
				\step[fieldset=editor, null]
				\step[fieldset=publisher, null]
				\step[fieldset=series, null]
				\step[fieldset=number, null]
				\step[fieldset=pages, null]
				\step[fieldset=volume, null]
			}
		}
	}
	\DeclareSortingTemplate{ymddnt}{
		\sort{
			\field{presort}
		}
		\sort[final]{
			\field{sortkey}
		}
		\sort[direction=descending]{
			\field{sortyear}
			\field{year}
			\literal{9999}
		}
		\sort[direction=descending]{
			\field{month}
			\literal{9999}
		}
		\sort[direction=descending]{
			\field{day}
			\literal{9999}
		}
		\sort{
			\field{sortname}
			\field{author}
			\field{editor}
			\field{translator}
			\field{sorttitle}
			\field{title}
		}
		\sort{
			\field{sorttitle}
			\field{title}
		}
	}
	%
	% We don't want to print the date where it normally appears.
	\renewbibmacro*{date}{}
	\renewbibmacro*{issue+date}{}
	% Field format for two digit numbers
	\DeclareFieldFormat{twodigits}{\ifnum#1<10 0#1\else #1\fi}
	% Instead, we want to print the bibliography as a list that has the year in the left column (in blue),
	% and the rest of the entry in the other.
%	\defbibenvironment{bibliography}{%
%		\list{\textcolor{accent}{\printfield[twodigits]{month}\iflanguage{ngerman}{.}{/}\printfield{year}}}{
%			\setlength{\labelwidth}{\descriptionwidthi}%
%			\setlength{\labelsep}{\descriptionsepi}%
%			\setlength{\itemsep}{\bibitemsep}%
%			\leftmargin\labelwidth%
%			\advance\leftmargin\labelsep%
%		}%
%		\sloppy\clubpenalty4000\widowpenalty4000%
%	}{\endlist}{\item}
	\defbibenvironment{bibliography}{%
%		\sloppy\clubpenalty4000\widowpenalty4000
		\begin{description}
	}{
		\end{description}
	}{
		\item[{\iffieldundef{month}{}{\printfield[twodigits]{month}\iflanguage{ngerman}{.}{/}}\printfield{year}}]
	}
}
