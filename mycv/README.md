# mycv.cls

Class I use for printing my own CV.
The class sets some format settings (which are, admittedly, inspired by moderncv) and provides some useful commands.
Features:

* A title section with your name, a subtitle (e.g., profession), and space for contact details and a photo.
* A tabular form, with dates or ranges of dates in the first column.
* Some stylistic choices that I think make sense. But that's up to you.
* if `biblatex` is loaded, the package adapts the style of the bibliography to print the year in the first column.

See `example.pdf` and `example.tex` for an impression and for how to use the file.
