# mytheorem-enum
This package provides `amsthm` with the feature to have nicely enumerated sub-theorems.

If this package is loaded with the option `patch-newtheorem`, then 

    \newtheorem{theorem}{Theorem}
	\begin{theorem}\label{thm:a}
		TFAE:
		\begin{enumerate}
			\item $a = b$ \label{thm:a1}
			\item $b = a$ \label{thm:a2}
		\end{enumerate}
	\end{theorem}
    A reference to \cref{thm:a1,thm:a2}

will produce what you expect: *A reference to Theorems 1(i) and 1(ii)*.
If patching `\newtheorem` is not an option, you can omit the option and instead call
`\InstallTheoremEnumerate{theorem}` for every environment `theorem` that you want to endow
with this kind of enumeration environment.

To refer to the subtheorems inside the proof, you may use `\proofref{...}` to produce 
just (i) instead of 1(i) or Theorem 1(i).

To change the appearance of the enumerations, renew `\ThEnumFormat`; for example,

    \renewcommand\ThEnumFormat{\textup{(\textit{\alph*})}}

will change the enumeration to Theorem 1(a).
