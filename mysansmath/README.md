# mysansmath
This package mildly extends the package `sansmathfonts`, which has to be installed.
Specifically, `mysansmath` provides two new math versions `sans` and `boldsans` matching Latin Modern, 
that can be activated using `\mathversion{sans}` and `\mathversion{boldsans}`, respectively.
The package does not change the global math font (unless, of course, `\mathversion{sans}`
is called in global scope).
It uses the math fonts installed with `sansmathfonts`.
If `amssymb` is loaded, the package also defines a (bold) sans math version for these fonts.

