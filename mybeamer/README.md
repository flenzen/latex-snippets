# mybeamer
This file contains a macros that I use when doing presentations.  The following lists the macros and how to use them.

## `\phalt<…>[…]{…}`
The command behaves like beamer's `\alt` command, i.e., if you write `\phalt<overlay-spec>{A}{B}`, the command shows `B`, and changes to `A` according to the `overlay-spec`.
Unlike `\alt`, `\phalt` always occupies the width/height/depth of its widest/highest/deepest argument.  With the optional argument, you may provide the alignment of the 
narrower argument inside this fixed-width box by specifying `\phalt<…>[c]{…}` (default), `\phalt<…>[r]{…}` or `\phalt<…>[l]{…}`. Example:
```
\begin{equation}
	\phalt<+->[r]{A}{B+C} = \phalt<.->{3}{4+4}
\end{equation}
```

## `\phAlternate[…]{…}`
Behaves like `\phalt`, but allows more overlays. Usage is best illustrated through an example:
```
\phAlternate[c]{ <1>{a} <2-3>{c} <4->{d} }
```
The mandatory argument takes a series of `<overlay-spec>{concents}`-pairs. It shows the `cotents` during the specified `overlay-spec`.
The command wraps its contents in a box of the width/height/depth of its widest/highest/deepest argument, just as `\phalt`.
The optional argument works the same way as for `\phalt`.
It's up to the user to make sure the overlay specifications don't overlap.

## `\mycallout<…>(…){…}{…}`
An overlay-aware callout-box. I don't remember where I got this code. Usage:
```
\tikzmark{node name}
\mycallout<overlay-spec>(angle:distance){node name}{Content}
```
The `(angle:distance)` argument is optional.

## Further commands
* automatic section title pages
* overlay-aware tikz styles `alt`, `visible on`, `onslide`, `temporal`. E.g., one may write `\tikz\draw[visible on=<2->, alt=<3->{blue}{red}] (0,0) -- (1,0);`

See `mybeamer.sty` for details.

