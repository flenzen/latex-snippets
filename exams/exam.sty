\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{exam}

% This package provides functionality to generate exams. 
% Environments:
%		problem: problem assignments (incl. points)
% Commands:
% 	\solution: space (grid) for solution, unveiling sample solution
% 	\gradinggrid: automatic grid for grading on title page
% Options
% 	solution: show solution
%	nogrid: when not showing solution, draw box instead of grid
%   novfillboxes: don't fill available vertical space

\newif\ifnovfillboxes
\newif\ifnogrid
\newif\ifloesung
\DeclareOption{novfillboxes}{\novfillboxestrue}
\DeclareOption{solution}{\loesungtrue}
\DeclareOption{nogrid}{\nogridtrue}
\ProcessOptions\relax

\makeatletter
\usepackage{xparse,calc,translations,sansmath}

\DeclareTranslation{english}{Problem}{Problem}
\DeclareTranslation{english}{points}{points}
\DeclareTranslation{english}{correction}{correction}
\DeclareTranslation{english}{of}{of}
\DeclareTranslation{english}{Solution}{Solution}
\DeclareTranslation{ngerman}{Problem}{Aufgabe}
\DeclareTranslation{ngerman}{points}{Punkte}
\DeclareTranslation{ngerman}{correction}{Korrektur}
\DeclareTranslation{ngerman}{of}{von}
\DeclareTranslation{ngerman}{Solution}{Lösung}
% General formatting.

\renewcommand{\titlepagestyle}{empty}

\newcommand{\punkte}[1]{\hfill\linebreak[3]\hspace*{\fill}\mbox{(\textit{#1~Punkte})}\ignorespaces}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%% PROBLEM ASSIGNMENTS %%%%%%%%%% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Environment for printing problem assignments.
\newcounter{problem}
\newenvironment{problem}[2][]{%
	\par%
	\if\relax\detokenize{#1}\relax
		\def\numOfPoints{#2}
	\else
	  \def\numOfPoints{#1}
	\fi
	\stepcounter{problem}%
	\savenumberofpoints{\numOfPoints}%
	\textbf{\GetTranslation{Problem} \arabic{problem} \hfill #2 \GetTranslation{points}}%
	\nopagebreak%
	\par%
}{\par\vspace{2em}}

% Store and read the number of points per problem to/from the aux file.
\newcommand{\savenumberofpoints}[1]{
	\ifnum #1=\getpoints{\theproblem}\relax
	\else
		\PackageWarning{exam}{Points for problem no. \theproblem\ have changed. Rerun LaTeX.}
	\fi
	\immediate\write\@auxout{%
		\noexpand\global\noexpand\@namedef{numberofpoints@problem@\arabic{problem}}{#1}%
	}
}
\newcommand{\getpoints}[1]{%
	\ifcsname numberofpoints@problem@#1\endcsname%
		\csname numberofpoints@problem@#1\endcsname%
	\else%
		0%
		\PackageWarning{exam}{No points available for problem no. #1.}%
	\fi%
}

% Store and read the number of problems to/from the aux file.
\AtEndDocument{
	\ifnum\theproblem=\getnumberofproblems\relax
	\else
		\PackageWarning{exam}{Number of problems has changed. Rerun LaTeX.}
	\fi
	\immediate\write\@auxout{%
		\noexpand\global\noexpand\@namedef{number@of@problems}{\arabic{problem}}%
	}
}
\newcommand{\getnumberofproblems}{%
	\ifcsname number@of@problems\endcsname%
		\number@of@problems%
	\else%
		0%
	\fi%
}

% Prints a grading grid with the appropriate number of problems and points to obtain.
\newcommand\addtabtoks[1]{\@tabtoks\expandafter{\the\@tabtoks#1}}
\newcommand*\resettabtoks{\@tabtoks{}}
\newcommand*\printtabtoks{\the\@tabtoks}

\newcommand{\gradinggrid}{
	% Build the tokens for the grid and store in \@tabtoks
	\newtoks\@tabtoks
	%
	% Headline
	\addtabtoks{\hline \GetTranslation{Problem}}%
	\count@=0%
	\loop\ifnum\count@<\getnumberofproblems\relax%
		\advance\count@ by 1%
		\expandafter\addtabtoks\expandafter{\expandafter&\number\numexpr\the\count@+1\relax}%
	\repeat%
	\addtabtoks{&$\Sigma$\\\hline 1. \GetTranslation{correction}}%
	%
	% First correction
	\count@=0%
	\loop\ifnum\count@<\getnumberofproblems\relax%
		\advance\count@ by 1%
		\addtabtoks{&\hspace*{\dimen0}}%
	\repeat
	%
	% Second correction
	\addtabtoks{&\hspace*{\dimen0}\\\hline 2. \GetTranslation{correction}}%
	\count@=0%
	\loop\ifnum\count@<\getnumberofproblems\relax%
		\advance\count@ by 1%
		\addtabtoks{&}%
	\repeat%
	% 
	% Of # points
	\addtabtoks{&\\\hline\GetTranslation{of}}%
	%
	\count@=0%
	\@tempcnta=0 %Total points
	\loop\ifnum\count@<\getnumberofproblems\relax%
		\advance\count@ by 1%
		\addtabtoks{&}%
		\expandafter\addtabtoks\expandafter{\expandafter\getpoints\expandafter{\the\count@}}%
		\advance\@tempcnta by \getpoints{\the\count@}%
	\repeat%
	\addtabtoks{&\the\@tempcnta\\\hline}%
	%
	% Build the actual table
	\renewcommand{\arraystretch}{1.8}%
	\dimen0=25pt%
	\begin{tabular}{|r|*{\numexpr\getnumberofproblems+1\relax}{c|}}
		\printtabtoks
	\end{tabular}
}

\newcommand{\totalNumberOfPoints}{%
	\count255=0%
	\newcount\totalpoints%
	\loop\ifnum\count255<\numexpr\getnumberofproblems\relax%
		\advance\count255 by 1%
		\advance\totalpoints by \getpoints{\the\count255}%
	\repeat%
	\the\totalpoints%
}


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%% BOXES %%%%%%%%%%%%%%%%% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Remaining space on the current page.
\def\restofpage{\dimexpr\pagegoal-\pagetotal-\baselineskip\relax}

% Save available space of current group to the aux file.
\newlength\restofpage@lastrun
\newlength\length@to@store
\newcounter{vfill@write@groups}
\newcommand{\saverestofpage}{
	\stepcounter{vfill@write@groups}%
	
	% Name of the macro the rest of the page is to be saved to.
	\def\length@name{restofpage@\roman{vfill@write@groups}}%

	% Rest of this page from last compilation. If undefined or if reset mode,
	% set this to 0pt.
	\ifnovfillboxes
		\setlength{\restofpage@lastrun}{0pt}
	\else
		\expandafter\ifcsname\length@name\endcsname
			\expandafter\expandafter\expandafter\restofpage@lastrun\expandafter\expandafter\expandafter=\expandafter\csname\length@name\endcsname
		\else
			\PackageWarning{exam}{No defined space for group \length@name{restofpage@\roman{vfill@write@groups}}.}
			\setlength{\restofpage@lastrun}{0pt}
		\fi%
	\fi%
	\PackageInfo{exam}{Rest of vfill-group no. \roman{vfill@write@groups}: \the\restofpage@lastrun}
	
	% If there is space left on the current page, make, make LaTeX rerun.
	\newif\ifno@rerun@necessary
	\no@rerun@necessaryfalse
	\ifdim\restofpage<3pt\relax
		\ifdim\restofpage>-1pt\relax
			\no@rerun@necessarytrue
		\fi
	\fi
	\ifno@rerun@necessary
		% Available space has almost not changed; neglect change to make aux file
		% become stationary.
		\immediate\write\@auxout{%
			\noexpand\global\noexpand\@namedef{\length@name}{%
				\the\dimexpr
					\restofpage@lastrun
				\relax
			}
		}
	\else
		% Save new rest of page = old rest of page + current rest of page.
		% Do nothing if there are no vfillboxes
		\ifdim\thevfillboxes pt=0pt
		\else
			\PackageWarning{exam}{\the\restofpage left to distribute on the \thevfillboxes boxes of group \roman{vfill@write@groups} on page \thepage. Rerun LaTeX.}
			\immediate\write\@auxout{%
				\noexpand\global\noexpand\@namedef{\length@name}{%
					\the\dimexpr
						\restofpage / \thevfillboxes + \restofpage@lastrun
					\relax
				}
			}
		\fi
	\fi
	
	\PackageInfo{exam}{\the\restofpage\space to be distributed over \thevfillboxes\space boxes of group \roman{vfill@write@groups} on page \thepage next run.}
	\setcounter{vfillboxes}{0}
	\setlength{\remainingspace@perbox}{\baselineskip}
	\global\remainingspace@perbox=\remainingspace@perbox
}

% Read available space for the next group from the aux file.
\newlength\remainingspace@perbox
\newcounter{vfill@read@groups}
\newcommand{\getrestofpage}{
	% Define macros for the names values for the current page are to be read from.
	\stepcounter{vfill@read@groups}
	\def\length@name{restofpage@\roman{vfill@read@groups}}
	
	% Set \remainingspace to the value of the length named by \length@name
	\ifnovfillboxes
		\setlength{\remainingspace@perbox}{0pt}
	\else
		\setlength{\remainingspace@perbox}{
			\expandafter\ifcsname\length@name\endcsname%
				\expandafter\csname\length@name\endcsname
			\else
				0pt
			\fi
		}
	\fi%
	
	% Make setting global
	\global\remainingspace@perbox=\remainingspace@perbox%
}

\newcommand\boxpagebreak{
	\ifnopagebreaks
	\else
		\saverestofpage
		\pagebreak
		\getrestofpage
	\fi
}
\newcounter{vfillboxes}


% Produce a box that consumes all available space,
% shared evenly among all boxes of this group.
% Optional: 
% - additional proportiality factor for height (default: 1),
% - width default: all available.
\NewDocumentCommand{\vfillbox}{O{1}O{\dimexpr\linewidth-2\fboxrule-2\fboxsep\relax}+m}{%   
	\par%
	\addtocounter{vfillboxes}{#1}%
	\framebox{%
		\parbox[t][#1\remainingspace@perbox][t]{#2}{\strut%
			#3%
		}%
	}%
}

\usepackage{xintfrac}
\NewDocumentCommand{\vfillgrid}{O{1}O{\linewidth} m}{%
	\par%
	\addtocounter{vfillboxes}{#1}%
	% Spacing of the grid, such that linewidth is an integer number of cells.
	\dimen0=\dimexpr#2/\xintiFloor{#2/\dimexpr 5mm\relax}\relax%
	% height of maximal integer number of cells that fit into the box.
	\dimen1=\dimexpr\dimen0*\xintiFloor{#1*\remainingspace@perbox/\dimen0}\relax%
	% residue height.
	\dimen2=\dimexpr\remainingspace@perbox - \dimen1\relax%
	\begin{tikzpicture}[baseline=-\the\dimen2]%
		\draw[step=\the\dimen0, ultra thin,gray] (0,0) grid (#2, \the\dimen1);
		\node[align=left, text width=\the\dimexpr#2-6.25pt\relax, anchor=north west, shape=rectangle, inner sep=3pt, font=\sffamily\sansmath] at(0, \the\dimen1) {#3};
	\end{tikzpicture}%
}

% Auto-sized box that prints or hides the sample solution.
% Optionally prints some text into the box to be completed by candidate.
% Usage:
% \loesungsbox
% 	[vertical space ratio w/r/t other boxes on the page; default one]
%	[text to be completed by student.]
%	{solution (or empty)}
\ifnovfillboxes
	\renewcommand\vfillbox[2][1]{\par\fbox{\parbox{\dimexpr\linewidth-2\fboxrule-2\fboxsep\relax}{#2}}}%
	\PackageInfo{exam}{Using frameboxes instead of vfillboxes}%
\else
\fi
\NewDocumentCommand{\solution}{O{1}+O{}+m}{%
	\par%
	\ifloesung%
		\ifnovfillboxes%
			{%
				\sloppy%
				\sffamily%
				\sansmath%
				\let\mathit\mathsfit%
				\textit{\GetTranslation{Solution}: }%
				#3%
			}%
		\else
			\vfillbox[#1]{%
				\sloppy%
				\sffamily%
				\sansmath%
				\let\mathit\mathsfit%
				\textit{\GetTranslation{Solution}: }%
				#3%
			}%
		\fi%
	\else%
		\ifnovfillboxes%
		\else%
			\ifnogrid%
				\vfillbox[#1]{#2}%
			\else%
				\vfillgrid[#1]{#2}%
			\fi%
		\fi%
	\fi%
}

\makeatother
