#!/usr/bin/env zsh

# Run latex for each of the participants, passing ID, family and given name to the the tex file.
# Rerun latex as long as the output reads the words "rerun latex"

OUTDIR=exam
mkdir -p $OUTDIR
while read PARTICIPANT FAMILY GIVEN; do
	for TYPE in exam solution ; do
		echo "Compile $TYPE for participant $PARTICIPANT" >&2
		while true; do
			LOG=$(
				pdflatex -output-directory=$OUTDIR -jobname="$TYPE-$PARTICIPANT" -interaction scrollmode <<- EOTEX
					\\def\\participant{$PARTICIPANT}
					\\def\\familyname{$FAMILY}
					\\def\\givenname{$GIVEN}
					\\def\\type{$TYPE}
					\\PassOptionsToPackage{nocolor}{random-identifiers}
					\\input{example.tex}
				EOTEX
			)
			if [[ $? -ne 0 ]]; then
				echo "Some latex compilation error occurred for participant $PARTICIPANT. Abort" >&2
				echo $LOG
				break
			fi
			if (echo $LOG | grep -iq "rerun"); then
				echo "Compile again"
			else
				break
			fi
		done
	done
done \
<<- EOL
	00000001 Doe			John
	00000002 Doe			Jane
	00000003 Carpenter		Alice
EOL


