# Exam suite
This package consists a few style and class files I used in the past to design exams.
The files are 

* `sheet.cls`, 
* `exam.sty`,
* `random-identifiers.sty`,
* `generate_exams.sh`

The following explains the use case.
An example can be found in `example.tex` and the folder `example`.

## *sheet* document class
This is just a wrapper arount `scrartcl.cls` that does some formatting changes that (I think) are suitable for problem assignments.
The file should be self-explanatory.

## *exam* package
This package provides

* an environment `problem` to wrap problems into.
  A problem gets as mandatory argument the number of points, and as optional argument how this should be displayed, e.g.
  ```
  \begin{problem}{4}[1+3]
  ...
  \end{problem}
  ```
  for a problem with 1 point for the first part and 3 points for the second.
* a command `\gradinggrid` that prints a schematic to write the achieved points per problem into,
  as is typically found on the first page of an exam.
  The command prints the points from the `problem` environments.
* a command `\solution[...][...]{...}` that produces a box for the participant to write the solution into.
  This are gridded by default; to suppress this, pass the option `nogrid` to the package.
  
  Available space on a page is distributed equally among the `solution` boxes.
  For this to work, one has to start pages by `\getrestofpage` and end pages by `saverestofpage`.
  In the middle of the document, one therefore usually finds
  ```
  \saverestofpage
  \pagebreak
  \getrestofpage
  ```
  
  Arguments to \solution:
  * If `\solution` receives the first optional argument *n*, 
  it gets *n* times as much space as the ones with no first optional argument.
  The factor *n* has to be an integer.
  * The second optional argument can be used to show a text to the participant to be completed.
  * The mandatory argument contains text that is shown instead as a solution if the option `solution` is given to the package.
    For example, 
    ```
    \begin{problem}{1}
      How much is $1 \cdot 2$?
      \solution[1][$1 \cdot 2 = {}$]{$1 \cdot 2 = 2$}
    \end{problem}
    ```
  If the package option `solution` is given, the mandatory argument is shown.

## *random-identifiers* package

The style file `random-identifiers.sty` can be used to randomize parts of an exam  (or any document, of course).
The main two functions of this package are

 1. Randomization of (groups of) text fragments
 2. Randomization of the order of parts of a task
 
### Randomization of text fragments
In mathematics (and similar subjects), one often finds the situation that one would like to change small details
of the assignment for each participant.
This could be variable names (“Let $V$ be a vector space and $U \subseteq V$ be a subspace” 
versus “let $U$ be a vector space and $V \subseteq U$ be a subspace”), or the concrete values of items in an assignment
(“compute the inverse of the matrix…”).
To achieve the first example, one can issue

```
\RandomizedIdentifiers{
	Space:    U, V;
	Subspace: V, U
}
```
this defines two macros `\Space` and `\Subspace`.
Each of the macros expands to the *n*th item of the list after the colon,
where *n* is an integer chosen randomly.
The lists with the alternative must have equal length.

One can then write 
```
Let $\Space$ be a vector space and $\Subspace$ be a subspace.
```
to use these two macros.
If the alternatives contain complicated code (even as complex as Ti*k*Z pictures is possible),
simply enclose each alternative in a pair of braces:
```
\RandomizedIdentifiers{
	RandomPicture: {\begin{tikzpicture}...\end{tikzpicture}}, {\begin{tikzpicture}...\end{tikzpicture}}
}
```

**Warning** Note that 
```
\RandomizedIdentifiers{Space:    U, V}
\RandomizedIdentifiers{Subspace: V, U}
```
is _not_ correct, because the two randomized assignments are treated independently.
Thus, with this code, one could end up with the situation that both `\Space` and `\Subspace` expand to the same variable name.

**Colors**
To make life easier when designing the exam, the default setting of the package is to highlight randomized variables.
When the exam is ready, switch this off by passing the option `nocolor` to the package.
One can also print *all* alternatives wherever a randomized variable occurs by specifying the option `alternatives` to the package.

Note that the colorizing mechanism may break some code.
To use `\RandomizedIdentifiers` with code that does not like the coloring, use `\RandomizedIdentifiers*`,
which will never produce colors.
Calling `\RandomizedIdentifiers*` also expands the values of the alternatives immediately by `\edef`ining the macros instead of `\def`ining them.
As an example, the following draws a regular $N$-gon, where $N$ is chosen randomly from given values:
```
\RandomizedIdentifiers*{N: 5,6,7,12}
\begin{tikzpicture}
	\node[draw=black,minimum size=2.5cm,regular polygon,regular polygon sides=\N] (a) {};
	\foreach \x in { 1, 2,..., \numexpr\N}{
		\node[fill=white, draw, inner sep=2pt, shape=circle] at (a.corner \x) {\x};
	}
\end{tikzpicture}
```


### Randomization of parts of a task
The other central task addressed by this package is randomization of the order of tasks.
When designing this package, we had in mind a task with several subtasks, whose order mostly does not matter.
However, it might be that some subtasks have to come before others, for example because they reuse the result of a previous one.

Therefore, the user has to provide a list of the admissible orders.
The package than chooses an order, and assigns macros for the tasks in that order.
For example, issuing
```
\RandomShuffleList{1,2,3; 1,3,2}{
	X: x, y, z;
	A: a, b, c;
```
defines two macros `\X` and `\A` that take one argument.
Calling `\X{1}`, `\X{2}` and `\X{3}` will either produce _x, y, z_ or _x, z, y_, because these are the prescribed allowed orders.
The contents of `\A` will have the matching order, i.e., either _a, b, c_ or _a, c, b_, respectively.
A use case for this would be randomized assignments such as
```
\RandomShuffleList{1,2,3; 1,3,2}{
	Assignments:
		{Compute the product $i = 2 \cdot 3$}, 
		{Is $i$ an even number?}, 
		{Is $i$ divisible by five?};
	Solutions:
		{$i = 6$}, Yes, No
}
```

## `generate_exams.sh`
To properly randomize the exams, one needs a seed.
The easiest way to do this is using the student ID.
This is done in `generate_exams.sh`.
The file shows how to use a list of participant names and student IDs and pass them to pdflatex
to compile an individualized exam.
The exam `example.tex` prints the name on the titlepage, and passes the student ID as seed to the random number generator of pgf.
The script compiles for each participant an exam and the corresponding solution.
Specifying the same seed for both the exam and the solution makes sure that the random values in both documents match.