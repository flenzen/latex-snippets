\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{sheet}

\LoadClass{scrartcl}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{babel}
\RequirePackage{mathtools,sansmath}
\RequirePackage[shortlabels,inline]{enumitem}
\RequirePackage[margin=2cm, footskip=\dimexpr.25in+\dp\strutbox\relax]{geometry}
\RequirePackage[all]{nowidow}


% General appearance

% Narrow bold
\renewcommand{\bfseries}{\fontseries{b}\selectfont}
\DeclareMathAlphabet{\mathbf}{\encodingdefault}{\rmdefault}{b}{n}
\setlength\parindent{0pt}
\setlength\parskip{4pt}

% Lists
\setlist[enumerate,1]{label=\emph{\alph*})}
\setlist[enumerate,2]{label=\emph{\roman*})}
\setlist{
	leftmargin=\widthof{\textit{a})}+\labelsep,
	itemsep=4pt,
	parsep=2pt
}
\date{\vspace{-1.25cm}}
\setkomafont{subject}{\large}
\setkomafont{title}{\bfseries}
\setkomafont{subtitle}{\large}
\setkomafont{author}{\large}


% From https://tex.stackexchange.com/a/246695/27717
\RequirePackage{letltxmacro}
\LetLtxMacro\orgvdots\vdots
\LetLtxMacro\orgddots\ddots
\DeclareRobustCommand\vdots{%
  \mathpalette\@vdots{}%
}
\newcommand*{\@vdots}[2]{%
	% #1: math style
	% #2: unused
	\sbox0{$#1\cdotp\cdotp\cdotp\m@th$}%
	\sbox2{$#1.\m@th$}%
	\vbox{%
		\dimen@=\wd0 %
		\advance\dimen@ -3\ht2 %
		\kern.5\dimen@
		% remove side bearings
		\dimen@=\wd2 %
		\advance\dimen@ -\ht2 %
		\dimen2=\wd0 %
		\advance\dimen2 -\dimen@
		\vbox to \dimen2{%
			\offinterlineskip
			\copy2 \vfill\copy2 \vfill\copy2 %
		}%
	}%
}
\DeclareRobustCommand\ddots{%
	\mathinner{%
		\mathpalette\@ddots{}%
		\mkern\thinmuskip
	}%
}
\newcommand*{\@ddots}[2]{%
	% #1: math style
	% #2: unused
	\sbox0{$#1\cdotp\cdotp\cdotp\m@th$}%
	\sbox2{$#1.\m@th$}%
	\vbox{%
	\dimen@=\wd0 %
	\advance\dimen@ -3\ht2 %
	\kern.5\dimen@
	% remove side bearings
	\dimen@=\wd2 %
	\advance\dimen@ -\ht2 %
	\dimen2=\wd0 %
	\advance\dimen2 -\dimen@
	\vbox to \dimen2{%
		\offinterlineskip
		\hbox{$#1\mathpunct{.}\m@th$}%
		\vfill
		\hbox{$#1\mathpunct{\kern\wd2}\mathpunct{.}\m@th$}%
		\vfill
		\hbox{$#1\mathpunct{\kern\wd2}\mathpunct{\kern\wd2}\mathpunct{.}\m@th$}%
	}%
	}%
}
