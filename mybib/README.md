# mybib.sty

This package contains some general changes to biblatex.
Generally, I mostly use biblatex' default settings.
However, there are usually a few things that bother me:

* If the source contains a DOI, it is uniquely determined. So I think there's no point in an URL, ISBN or ISSN.
  Same is true if no DOI, but a ISBN is specified: no URL need.
  Therefore, the file defines a source map that removes URL (and ISBN, ISSN) if a DOI is present.
* Sometimes, articles have a DOI already, but aren't published yet (or under review, or…).
  To clarify this, the `howpublished` attribute in the `bib` is intended.
  But its content is appended to the entry.
  This style file (adapted from [stackexchange](https://tex.stackexchange.com/a/408041/101651)
  prints `howpublished={submittedto}` and `howpublished={toappearin}` as you might expect.
* Sometimes, we also include an arXiv ID with a reference that describes something else.
  This could be an article to appear, with the preprint on arXiv, or an extended version of a paper.
  To clarify this, use `usera={preprint}` and `usera={extendedversion}` in the bibfile
* To connect an entry to another it is an extended version of, use `related={key of other}`
  and `relatedtype={extendedversionof}`. If the other key is already in the bibliography,
  this will just print *Extended version of [citation]*. Otherwise, it will print *Extended version of:
  [abridged bibliographical data]*.